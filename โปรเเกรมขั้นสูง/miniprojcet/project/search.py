import cv2
import os
import numpy
import sys


surf = cv2.SURF(800, extended=False)

source = 'image'
bf = cv2.BFMatcher()
im = cv2.imread('test.jpg',0)
kp2, des2 = surf.detectAndCompute(im, None)
D = {}
for root, dirs, filenames in os.walk(source):
    for fs in filenames:
            img  = source + "/" + fs
            f = cv2.imread(img,0)
            kp, des = surf.detectAndCompute(f, None)
            matches = bf.knnMatch(des,des2, k=2)
            i = 0
            for m,n in matches:
                if m.distance < 0.4*n.distance:
                    i = i + 1
            D[fs] = i

def get_count(tuple):
    return tuple[1]

sortedlist = sorted(D.items(), key = get_count ,reverse=True)
stringss = 'data\\'+sortedlist[0][0]
im2 = cv2.imread(stringss,1)
cv2.imshow("Results", im2)
cv2.waitKey(0)
