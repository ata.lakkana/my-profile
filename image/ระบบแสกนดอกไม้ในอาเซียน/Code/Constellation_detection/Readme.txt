Readme file of constellation detection  --Xiaoge Liu, Suyao Ji, Jinzhi Wang 20160605


1.Main Detection file
    detection.m
  (a)subfunction
	preprocessing.m      %binary the image
	Template_frst	     %generate the search list of constellations
	areaFilter           %remove extremely small region or extremely large region
	StarIsOverlop        %compare two match pattern wheterh they have shared star
 	export_fig  	     %export fig
  (b)load data
	ConstDatabase 
  (c)save data
	TestImgResult

2.Create template databse
    	DatabaseGen.m
  (a)subfunction
 	Constel_Info_Grap.m  %grab information from template image
	list_dir.m           %grab name of testimage in the folder
  (b)load data
	testimage in the testimage folder
	Large_table_of_88_constellations_and_their_neighbors_with_normalized_dist.mat
  (c)save data
	ConstDatabase.mat
	