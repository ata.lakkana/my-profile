function Result=StarIsOverlop(Check_Star,Corr_Consel_Star)
%%determine whether there is a star overloping between correct
%%constellation and checking constellation
tol=60;
Result=1;
for nCStar=1:size(Check_Star,1)   %%check all the detected star in this constellation
        Star=Check_Star(nCStar,:);
         for nCorrStar=1:size(Corr_Consel_Star,1)                               %%compare this star with stars in correct constellation
                        CorrStar=Corr_Consel_Star(nCorrStar,:) ;           
                        if sum(abs(Star-CorrStar))<tol             % if the star overlap, this constellation is wrong.
                           Result=0;
%                            disp(sprintf('Name=%s ..%d, \n Star=%d %d\n  CheckStar=%d %d\n',ConstDatabase{Detected_name(nCheck),1},nCheck,Star,CorrStar));
                        end
             end
    end
end