clear all, close all, clc;
dataFolders = '88 constelation maps';
imageFiles = list_dir(dataFolders, '.gif'); % list_dir, function by Hang
numFiles = length(imageFiles); % number of files in folder
ConstDatabase = cell(numFiles,1); % initialize final output cells
% CDBCelllength = 2; % filename + matrix of coordinates of const. image to store in cell
% infoCell = cell(1, numFiles); % initialize cell to store coordinates
load('Large_table_of_88_constellations_and_their_neighbors_with_normalized_dist.mat');
NeBourInfo=table2cell(table);
for i = 1:numFiles
    disp(sprintf('%d..%d',i,numFiles));
    Constellation = Constel_Info_Grap(imageFiles{i}); % for each CDB cell, it is a cell w/ length of CDBCelllength
    
    ConstDatabase{i,1}=Constellation{1};
    ConstDatabase{i,2}=Constellation{2};
    ConstDatabase{i,3}=Constellation{3};
    %find the neighbour constel;
    [path,name,ext]=fileparts(imageFiles{i});
    find=0;
    for nNB=1:size((NeBourInfo),1)
       if strcmp(name,NeBourInfo{nNB,1})
           ConstDatabase{i,4}=NeBourInfo{nNB,10};
           ConstDatabase{i,5}=NeBourInfo{nNB,2};
           find=1;
           for index=6:10
             ConstDatabase{i,index}=NeBourInfo{nNB,index-1};
             
           end
       end
    end
    if find==0
        disp(sprintf('warning No neighbour information of constellation %s',name));
    end
   
end % i,numFiles



[row,column]=size(ConstDatabase);
for nCStel = 1:row
    for nNB=5:column
        if ConstDatabase{nCStel,nNB}
            name=ConstDatabase{nCStel,nNB};
        for nCheck = 1:row
         if strcmp(name, ConstDatabase{nCheck,1},'exact');
           ConstDatabase{nCStel,nNB}=nCheck;
           break;
         end
        end
       end
    end    
end
EmIndex = cellfun('isempty',ConstDatabase(:));
ConstDatabase(EmIndex)={NaN};
save('ConstDatabase.mat','ConstDatabase');
% return ConstDatabase

%%connect the result with visible mag and neighbour constellation.


