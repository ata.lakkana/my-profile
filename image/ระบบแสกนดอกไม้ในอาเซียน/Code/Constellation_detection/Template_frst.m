function list=Template_frst(ConstDatabase);
%%generate the first list of template row number that we want to check in
%%the frst round
if nargin==0
    load('ConstDatabase.mat');
end
ConstNum=size(ConstDatabase,1);
for nIdx=1:ConstNum
       starProp(nIdx,1) = size(ConstDatabase{nIdx,2},2);
       starProp(nIdx,2) = (1/2.5)^ConstDatabase{nIdx,5};
end
[A,index]=sortrows(starProp,[-1,-2]);
list=index;
end