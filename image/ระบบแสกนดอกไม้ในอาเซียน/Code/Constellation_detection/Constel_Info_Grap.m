function constel_info=Constel_Info_Grap(imagefilename,num)
% 20150510-Xiaoge Liu
%constel_info=Constel_Info_Grap(imagefilename,num):this function grap the constellation information from a constellation
%image. imagefilename is the string that contains the path and filename.
%constel_info is a cell of 1*2. The first is the name obtained from the
%filename, the second is a 3*num matrix. num is the number of stars we collect. 1 column is the brightest star. 1
%row is x location, and 2 row is the y location, 3 row is the area. The
%area are normalized to the brightes star. The x,y is scaled and rotate, so
%the location of first brightest star is (0,0). and the second brightest star is
%(1,0). the third cell is the matrix that contain all the connection line
%info. 4*numLine matrix. 
% Row 1 is x location of left end and row 2 for y
%location. Row 3 is x location of right end and row 4 is y location

% Check number of inputs.
switch nargin
    case 0
        imagefilename='88 constelation maps\UrsaMajor.gif';
        num=20;
    case 1
        num=20;
    case 2
        ;
    otherwise 
        error('requires at most 2 optional inputs');
end

 [path,name,ext]=fileparts(imagefilename); 
 [img,Imap]=(imread(imagefilename));
 imgRGB=ind2rgb(img,Imap);

 imgR=imgRGB(:,:,1);
 imgB=imgRGB(:,:,3);
 t1=0.65;t2=0.75;
 imgBW=(imgR>t1).*(imgR<t2);
 imgStar=areaFilter(imgBW, 3300, 10);
 
%   figure();
%  imshow(imgStar);
 imgLabel = bwlabel(imgStar);
 shapeProps = regionprops(imgLabel, 'Area');
 num=min(length(shapeProps),num);
 star=zeros(length(shapeProps),3);
 t3=0.4;t4=0.8;
 imgBWLine=(imgB>t3).*(imgB<t4);
 imgLine=areaFilter(imgBWLine, 2000, 15);
 imgLabelLine = bwlabel(imgLine,8);
 shapePropsLine = regionprops(imgLabelLine, 'Area');
 numLine=min(length(shapePropsLine),num);
 Line=zeros(length(shapePropsLine),5);

 imgStarForLine=areaFilter(imgBW, 3300, 5);
 imgLabelStarForLine = bwlabel(imgStarForLine);
 %  figure()
%  imshow(imgLine);
%  imshow(imgLabel);
% figure;
% subplot(1,2,1);
% imshow(imgRGB(:,:,1));
% subplot(1,2,2);
% imhist(imgRGB(:,:,1));
% figure;
% subplot(1,2,1);
% imshow(imgRGB(:,:,2));
% subplot(1,2,2);
% imhist(imgRGB(:,:,2));
% figure;
% subplot(1,2,1);
% imshow(imgRGB(:,:,3));
% subplot(1,2,2);
% imhist(imgRGB(:,:,3));
%  hold on
se = strel('square',20);
for nStar=1:1:length(shapeProps)
    imgLabelnStar=(imgLabel == nStar);
    [y,x] =find(imgLabelnStar==1);
    centerx=mean(x);
    centery=mean(y);
    star(nStar,1)=shapeProps(nStar).Area;
    star(nStar,2)=centerx;
    star(nStar,3)=centery;
%   scatter(centerx,centery,star(nStar,1),'filled','r');
%    figure();
%    imshow(imgLabelnStar);
end
for nStarForLine=1:1:max(imgLabelStarForLine(:))
   imgLabelnStarForLine=(imgLabelStarForLine == nStarForLine);
   for nLine=1:1:length(shapePropsLine)
    imgLabelnLine=(imgLabelLine == nLine);
    imgLabelnLineDila=imdilate(imgLabelnLine,se);
%     imgLabelnStarDila=imdilate(imgLabelnStar,se);
    imgOverlap=min(imgLabelnLineDila,imgLabelnStarForLine);
    if sum(imgOverlap(:))>1
        %Line connect to this star
        [yline,xline] =find(imgOverlap==1);
        centerxline=mean(xline);
        centeryline=mean(yline);
        Line(nLine,5)=Line(nLine,5)+1;
        if  Line(nLine,5)>2
%             pause;
            Line(nLine,5)=2;
%             disp('one line connect more than two stars');
        end
        Line(nLine,Line(nLine,5)*2-1)=centerxline;
        Line(nLine,Line(nLine,5)*2)=centeryline;
     end
%     figure();
%     imshow(imgOverlap);
%     
end

end
% figure();
% imshow(imgStar);hold on;
% for nLine=1:1:length(shapePropsLine)
%     plot(Line(nLine,1:2),Line(nLine,3:4),'b','LineWidth',2);
% end
constel=sortrows(star,1);
constel=flipud(constel);
constel=constel(1:num,:);
ZeroCenterX=constel(1,2);
ZeroCenterY=constel(1,3);
constel(:,2)=constel(:,2)-ZeroCenterX;
constel(:,3)=constel(:,3)-ZeroCenterY;
theta=atan(-constel(2,3)/constel(2,2));
Scale=sign(constel(2,2))/norm(constel(2,2:3));
rotM=[Scale*cos(theta),-Scale*sin(theta);
      Scale*sin(theta),Scale*cos(theta)];
constel_XY=rotM*constel(:,2:3).';
constel_Area=(constel(:,1)/constel(1,1)).';
constel_Matrix=[constel_XY;constel_Area];

NumLine=0;
for nLine=1:1:size(Line,1)
    if Line(nLine,5)>=2;
        NumLine=NumLine+1;
        LineCut(NumLine,:)=Line(nLine,:);
    end
end
Line=LineCut;
constelLine=zeros(4,size(Line,1));
constelLine(1,:)=Line(:,1)-ZeroCenterX;
constelLine(2,:)=Line(:,2)-ZeroCenterY;
constelLine(1:2,:)=rotM*constelLine(1:2,:);
constelLine(3,:)=Line(:,3)-ZeroCenterX;
constelLine(4,:)=Line(:,4)-ZeroCenterY;
constelLine(3:4,:)=rotM*constelLine(3:4,:);

%show the grab result
figure();hold on;
imshow(imgStar);hold on;
for nStar=1:1:num
    scatter(constel_XY(1,nStar),-constel_XY(2,nStar),'blacko');
    text(constel_XY(1,nStar)+0.1,0.1-constel_XY(2,nStar),sprintf('%d=%.2f',nStar,constel_Area(nStar)));
end
for nLine=1:1:size(constelLine,2)
    plot([constelLine(1,nLine),constelLine(3,nLine)],[-constelLine(2,nLine),-constelLine(4,nLine)],'b','LineWidth',2);
end
%
constel_info=cell(1,3);
constel_info{1}=name;
constel_info{2}=constel_Matrix;
constel_info{3}=constelLine;
% pause; 

end
