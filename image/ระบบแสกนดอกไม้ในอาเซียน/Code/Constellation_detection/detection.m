clear all;
close all;

tol = 9;%  tolerance window to compare whether there is a star in the predict location


RankTolerance1st=0.8;%0.8;      %%tolerance of rank (total rank mismatch over num of match) for 1st constellation
ErrorTolerance1st=5;%4.5;     %%tolerance of shift error for 1st constellation


ScaleToleranceRest=0.085;    %%tolerance of scale once we find the first const
RankToleranceRest=0.6;      %%tolerance of rank (total rank mismatch over num of match)
ErrorToleranceRest=4.5;     %%tolerance of shift error
NUM_MATCH_MAX=5;          %%match number of stars in this const

%tune the parameter to get better result, in the future, we should limit
%the total number of stars on the image.And improve the accuracy and speed
%for 1st constellation detection.

%%
%load already generated constellation database
load('ConstDatabase.mat');
SearchList=Template_frst(ConstDatabase);
% SearchList=73;%[60,38,79,36,55]%[38,34,46,62,76,9,61,8]%,,1120,12
%%
%%Detection
tic;
Img=imread('TestImage/testimage1.png');
ImgArt=imread('TestImage/testimage1-refer.png');
Img_filter = preprocessing(Img);
% sort the bright stars by area
[starsLabel num_star] = bwlabel(Img_filter,8);
stars = regionprops(starsLabel, 'Area');
for i=1:num_star
    [r c] = find(starsLabel == i);
    star(i,3)=stars(i).Area;
    star(i,1)=(max(r)+min(r))/2;
    star(i,2)=(max(c)+min(c))/2;
end
star = sortrows(star,[-3]); 
match=zeros(1,89);
detected_num=0;
Detected_cst=0;
ScaleFrom1stCon=0;
for nSearch=1:length(SearchList)
     cst = SearchList(nSearch) %%number of constellation 
     match_rank=[];
     Error=[];
     [a temp_star_num]=size(ConstDatabase{cst,2});
     if detected_num==0   %%tolerance for first constellation
      
         RankTolerance=RankTolerance1st;      %%tolerance of rank (total rank mismatch over num of match)
         ErrorTolerance=ErrorTolerance1st;     %%tolerance of shift error
         NUM_MATCH=floor(temp_star_num*1/2-1); 
     else
         ScaleTolerance=ScaleToleranceRest;    %%tolerance of scale once we find the first const
         RankTolerance=RankToleranceRest;      %%tolerance of rank (total rank mismatch over num of match)
         ErrorTolerance=ErrorToleranceRest;     %%tolerance of shift error
        
        if temp_star_num<=3 % database has constellations which only has two or 3 points, ignore these constellations
           continue;
        end
        if temp_star_num<NUM_MATCH_MAX;
             NUM_MATCH=temp_star_num;
        elseif temp_star_num <12
            NUM_MATCH = NUM_MATCH_MAX;
        else
            NUM_MATCH =floor(temp_star_num*1/2-1);
        end
     end
     wrong_star_tol=temp_star_num-NUM_MATCH;
     match_area=zeros(1,NUM_MATCH);
     temp_stars = ConstDatabase{cst,2};
     for bright_1i =1:num_star
            % transform the template to the angle so that the test image and template
            % orientation is the same
            r0 = star(bright_1i,1); % brightest star
            c0 = star(bright_1i,2);
            for bright_2i=1:num_star % second brightest star loop
                  if (bright_2i ~=bright_1i)
                    r1 = star(bright_2i,1); % second brightest star
                    c1 = star(bright_2i,2);
                    dx = c1-c0;
                    dy = r0-r1;
                    L=sqrt(dx^2+dy^2);
                    if ScaleFrom1stCon~=0
                        CheckScale=L/ConstDatabase{cst,4};
                        if abs(CheckScale-ScaleFrom1stCon)>ScaleTolerance
                           continue;
                        end
                    end
                    rot_angle = (180*(1-sign(dx))/2 + atan(dy/dx)/pi*180);
                    rot_angle = rot_angle/180*pi;
                    rot_Matrix = L*[cos(rot_angle) sin(rot_angle);-1*sin(rot_angle) cos(rot_angle)];
                    temp_rot = rot_Matrix * temp_stars(1:2,:) + [c0*ones(1,temp_star_num); r0*ones(1,temp_star_num)];
                    % if the test image brightness ranking is different from the template (the
                    % 1st, 2nd are the same
                    itr=3;  %% number of detected star in the constellation
                    tsa(1:num_star)=1:num_star; %% reset the search star 
                    if (bright_1i<bright_2i)
                        tsa(bright_1i)=[];
                        tsa(bright_2i-1)=[];
                    else
                        tsa(bright_2i)=[];
                        tsa(bright_1i-1)=[];
                    end
                    i=1;
                    find_star=2;
                    wrong_star=0;
                    match_area(1)=star(bright_1i,3);
                    match_area(2)=star(bright_2i,3);
                    SumErrorX=0;
                    SumErrorY=0;
%for debuging using 
%                     figure;
%                     imshow(starsLabel);hold on;
%                     for liuxg=1:size(temp_rot(1,:),2)
%                     scatter(temp_rot(1,liuxg),temp_rot(2,liuxg),100,'redo');
%                     h=text(temp_rot(1,liuxg),temp_rot(2,liuxg),sprintf('%d',liuxg));
%                     set(h,'color','white','Fontsize',14);    
%                     end
                    while itr < temp_star_num                       
                        if abs(star(tsa(i),1)-temp_rot(2,itr))<=tol  && abs(star(tsa(i),2)-temp_rot(1,itr))<=tol
%                             scatter(star(tsa(i),2),star(tsa(i),1),100,'green*');
                            SumErrorX=SumErrorX+abs(star(tsa(i),1)-temp_rot(2,itr));
                            SumErrorY=SumErrorY+abs(star(tsa(i),2)-temp_rot(1,itr));
                            itr=itr+1;
                            find_star=find_star+1;
                            match_area(find_star)=star(tsa(i),3);
                            tsa(i)=[];
                            i=1;                      
                            
                        else
                            i=i+1;
                            
                            if i>length(tsa)
                                i=1;
                                itr=itr+1;
                                wrong_star=wrong_star+1;
                             end
                           
                        end
                        if find_star>=NUM_MATCH || wrong_star>wrong_star_tol;
                              break;
                        end
                    end
                    
                    if (find_star == NUM_MATCH)
                                           
                        [a, dindex] =sort(match_area,'descend');
                        rank=norm(dindex(1:NUM_MATCH)-(1:NUM_MATCH));
                        rank=rank/NUM_MATCH;
                        AveraError=sqrt(SumErrorX^2+SumErrorY^2)/NUM_MATCH;
                        if (rank<RankTolerance) && (AveraError<ErrorTolerance)
                            
                            ConstDatabase{cst,1}
                            match(cst)=match(cst)+1;
                            match_rank(match(cst))=rank;
                            star_name(match(cst)) = cst;
                            scale_tmp(match(cst)) = sqrt((r0-r1)^2+(c0-c1)^2)/ConstDatabase{cst,4};
                            RotatM_tmp{match(cst)}=[rot_Matrix;c0,r0];
                            Error(match(cst))=AveraError;
                        end
                    end
                end
            end
        end
    if (match(cst)>=1)
        Detected_cst=Detected_cst+1;
        [a,didx]=sort(Error);
        if length(didx)>4
            nError=4;
        else
            nError=length(didx);
        end
        for i=1:nError
            detected_num = detected_num+1;
            Detected_name(detected_num)=cst;
            Detected_Errer(detected_num)=Error(didx(i));
            Detected_rank(detected_num)=match_rank(didx(i));
            Detected_scale(detected_num) = scale_tmp(didx(i));
            RotatM{detected_num}=RotatM_tmp{didx(i)};
            
        end
    end
   if Detected_cst==1
       ScaleFrom1stCon=Detected_scale(1);
   end
  
end
%%
%plot the all detected constellation including the wrong ones.   
%  figure;
%  imshow(Img_filter);hold on

if detected_num==0
    error('No Constellation detected');
  end
 StarList=star(:,1:3);
 for NumCorr=1:length(Detected_name) 
 rot_Matrix=RotatM{NumCorr}(1:2,:);
 c0=RotatM{NumCorr}(3,1);
 r0=RotatM{NumCorr}(3,2);
 cst=Detected_name(NumCorr);
 ConstelStar=ConstDatabase{cst,2};
 StarNum=size(ConstelStar,2);
 StarProject{NumCorr}= rot_Matrix * ConstelStar(1:2,:) + [c0*ones(1,StarNum); r0*ones(1,StarNum)];
 StarOnImg=[];
 for nStar=1:StarNum
             tmp=abs(StarList(:,1)-StarProject{NumCorr}(2,nStar))+abs(StarList(:,2)-StarProject{NumCorr}(1,nStar));
             [Minvalue,findstar]=min(tmp);
             
             if Minvalue<10*tol
                 StarOnImg(nStar,1:3)=StarList(findstar,1:3);
             else
                  StarOnImg(nStar,1:3)=[-1,-1,1];
             end
 end
%  scatter(StarProject{NumCorr}(1,:),StarProject{NumCorr}(2,:),'redo','LineWidth',1.5);
%  scatter(StarOnImg(:,2),StarOnImg(:,1),'green*','LineWidth',1.5);
 
 ConstOnImg{NumCorr}=StarOnImg;
 
        LineOnImgRightEnd=[];
        LineOnImgLeftEnd=[];
        ConstelLine=ConstDatabase{cst,3};
        ConstelLineLeftEnd=ConstelLine(1:2,:);
        ConstelLineRightEnd=ConstelLine(3:4,:);
        LineNum=size(ConstelLine,2);
        LineOnImgLeftEnd = rot_Matrix * ConstelLineLeftEnd + [c0*ones(1,LineNum); r0*ones(1,LineNum)];
        LineOnImgRightEnd = rot_Matrix * ConstelLineRightEnd + [c0*ones(1,LineNum); r0*ones(1,LineNum)];       
%         for nLine=1:LineNum
%             plot([LineOnImgLeftEnd(1,nLine),LineOnImgRightEnd(1,nLine)],[LineOnImgLeftEnd(2,nLine),LineOnImgRightEnd(2,nLine)],'b','LineWidth',2);
%         end
        ConstLineOnImg{NumCorr}=[LineOnImgLeftEnd;LineOnImgRightEnd];
 end
 %%
  %%remove all the overlap constellation.  first, remove all the overlap points  All I have is
  %%Detected_name and RotatM
 NumCorr=1;
 NeighbourArray=1;
 Neighbournext=0;
 Check_name=Detected_name;
 Correct_name=zeros(1,length(Detected_name));
  while Neighbournext<length(NeighbourArray)
         Neighbournext=Neighbournext+1;
         NumCorr=NeighbourArray(Neighbournext);
         SameCst=find(Check_name==Check_name(NumCorr));
         if size(SameCst,2)>1
             Check_name(SameCst)=0;
         end
         Check_name(NumCorr)=0;
         Correct_name(NumCorr)=Detected_name(NumCorr);
         Corr_Consel_Star=[];
         Corr_Consel_Star=ConstOnImg{NumCorr}; %%chose the star for correct constellation
        for nCheck=1:length(Check_name)  %%check all the detected constellation
            if Check_name(nCheck)==0
                continue;
            end
        %          Check_name=Detected_name(nCheck);
            Check_Star=[];
            Check_Star=ConstOnImg{nCheck};
            Result=StarIsOverlop(Check_Star,Corr_Consel_Star);
            Check_name(nCheck)=Check_name(nCheck)*Result;
        end
        for Neigbour=6:10   %%find the neighbor constellation of correct constellation and add them into the NeighbourArray
              NextNeigbour=ConstDatabase{Detected_name(NumCorr),Neigbour};
              if (~isnan(NextNeigbour)) && any(Check_name==NextNeigbour)
                [index]=find(Check_name==NextNeigbour);
                if size(index,2)>1
                    [ErrorCheck,indexError]=min(Detected_Errer(index));
                    index=index(indexError);
                end
                NextNeighbour_Star=[];
                NextNeighbour_Star=ConstOnImg{index};  %%check whther the next neighour conflict this other correct constellations
                TotalCheck=1;
                for CheckConflict=1:length(NeighbourArray)
                         Check_Star=[];
                         Check_Star=ConstOnImg{NeighbourArray(CheckConflict)};
                         NextNeighbourResult=StarIsOverlop(Check_Star,NextNeighbour_Star);
                         TotalCheck=TotalCheck*NextNeighbourResult;

                end
                if TotalCheck==1
                         NeighbourArray=[NeighbourArray,index];
                end
              end
        end
           
  end

  
  %%

toc;

%%plot remove result
figure;
imshow(Img); hold on
% imshow(ImgArt);hold on
StarList=star(:,1:2);
for NumCorr=1:length(Detected_name) 
     if Correct_name(NumCorr)==0
         continue;
     end
 StarOnImg=ConstOnImg{NumCorr};
 ConstNum=Detected_name(NumCorr);
 ConstName=ConstDatabase{ConstNum,1};
 ConstStarArea=ConstDatabase{ConstNum,2}(3,:).*(1/2.5)^ConstDatabase{ConstNum,5};
 LineOnImgLeftEnd=ConstLineOnImg{NumCorr}(1:2,:);
 LineOnImgRightEnd=ConstLineOnImg{NumCorr}(3:4,:);  
 for nLine=1:size(LineOnImgLeftEnd,2)
            plot([LineOnImgLeftEnd(1,nLine),LineOnImgRightEnd(1,nLine)],[LineOnImgLeftEnd(2,nLine),LineOnImgRightEnd(2,nLine)],'b','LineWidth',2);
 end
 scatter(StarOnImg(:,2),StarOnImg(:,1),(StarOnImg(:,3)).^(0.25)*30,'green*');
 scatter(StarProject{NumCorr}(1,:),StarProject{NumCorr}(2,:),ConstStarArea(:).^(0.5)*400,'redo','LineWidth',1.5);
 
 CenterCons=mean(StarProject{NumCorr},2);
 h=text(CenterCons(1),CenterCons(2),ConstName);
 set(h,'color','white','Fontsize',14);     
 end%   

 %% 
 %%print out the result
   export_fig  TestResult -M4
