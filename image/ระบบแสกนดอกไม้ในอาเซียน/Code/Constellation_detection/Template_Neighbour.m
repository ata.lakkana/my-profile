function [list]=Template_Neighbour(Constel_Index,ConstDatabase);
%%generate the list of template constellations that are neighbours to the
%%constellation at constel_index 
if nargin==1
    load('ConstDatabase.mat');
else if nargin==0
        Constel_Index=1;
        load('ConstDatabase.mat');
    end
end
for n=5:9
list(n-4)=ConstDatabase(Constel_Index,n);
end
end