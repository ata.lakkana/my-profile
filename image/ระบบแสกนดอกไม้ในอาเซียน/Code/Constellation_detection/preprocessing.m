%% preprocessing the dark night image
function Img_filter = preprocessing(Img)

%converto the grey image and apply Ostu's method to binarize the image
Img_gray=rgb2gray(Img);
%threshold = graythresh(Img_gray)
threshold=0.5;
Img_bi = im2bw(Img_gray,threshold);
% figure;
% imshow(Img);
% figure;
% imshow(Img_bi);
% filter out small stars
se2=strel('square',2);
se3=strel('square',3);
Img_filter = imdilate(imerode(Img_bi,se2),se3);
Img_filter = Img_bi;
% figure;
% imshow(Img_filter);
